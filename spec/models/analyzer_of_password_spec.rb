require 'rspec'
require_relative '../../models/analyzer_of_password'

describe 'AnalyzerOfPassword' do
  let(:analyzer) { AnalyzerOfPassword.new }

  it 'should be true when the size of the password is longer than 10 and it has 4 numbers' do
    expect(analyzer.analyze_size('password1234')).to be_nil
  end

  it 'should throw an exception when the size of the password is less than 10' do
    expect { analyzer.analyze_size('pass') }.to raise_error RuntimeError
  end

  it 'should throw an exception when the numbers of the password are less than 4' do
    expect { analyzer.analyze_numbers('password33') }.to raise_error RuntimeError
  end

  it 'should throw an exception when the password do not match with other password' do
    expect { analyzer.compare_passwords('passwo3322', 'passwor332') }.to raise_error RuntimeError
  end
end
