require 'rspec'
require_relative '../../models/analyzer_of_remuneration'

describe 'AnalyzerOfRemuneration' do
  let(:analyzer) { AnalyzerOfRemuneration.new }

  it 'should be true when the remuneration is positive' do
    expect(analyzer.analyze_remuneration('20000')).to be_nil
  end

  it 'should throw an exception when the remuneration is negative' do
    expect { analyzer.analyze_remuneration('-17000') }.to raise_error RuntimeError
  end

  it 'should throw an exception when the remuneration contains chars' do
    expect { analyzer.analyze_remuneration('10as1') }.to raise_error RuntimeError
  end
end
