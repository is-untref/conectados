require 'spec_helper'

describe JobApplication do
  subject(:job_application) { described_class.new }

  describe 'model' do
    it { is_expected.to respond_to(:applicant_email) }
    it { is_expected.to respond_to(:description_postulant) }
    it { is_expected.to respond_to(:job_offer) }
  end

  describe 'job_application valid?' do
    it 'should be false when email is blank' do
      email = ''
      result = described_class.create_for(email, JobOffer.new, '', '11-11-1998')
      expect(result.validate_email(email)).to be_falsey
    end

    it 'should be false when email has not the at symbol' do
      email = 'applicant.com'
      result = described_class.create_for(email, JobOffer.new, '', '11-11-1998')
      expect(result.validate_email(email)).to be_falsey
    end

    it 'should be false when email has not the dot symbol' do
      email = 'applicant@com'
      result = described_class.create_for(email, JobOffer.new, '', '11-11-1998')
      expect(result.validate_email(email)).to be_falsey
    end
  end

  describe 'create_for' do
    it 'should set applicant_email' do
      email = 'applicant@test.com'
      result = described_class.create_for(email, JobOffer.new, '', '11-11-1998')
      expect(result.applicant_email).to eq(email)
    end

    it 'should set job_offer' do
      offer = JobOffer.new
      result = described_class.create_for('applicant@test.com', offer, '', '11-11-1998')
      expect(result.job_offer).to eq(offer)
    end

    it 'should set description_postulant' do
      description_postulant = 'Quisiera estar en su empresa'
      result = described_class.create_for('applicant@test.com', JobOffer.new, description_postulant, '11-11-1998')
      expect(result.description_postulant).to eq(description_postulant)
    end
  end

  describe 'process' do
    it 'should deliver contact info notification' do
      result = described_class.create_for('applicant@test.com', JobOffer.new, 'Hi', '11-11-1998')
      expect(JobVacancy::App).to receive(:deliver).with(:notification, :contact_info_email, result)
      expect(JobVacancy::App).to receive(:deliver).with(:notification,
                                                        :description_info_email, result)
      result.process
    end
  end
end
