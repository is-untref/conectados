require 'spec_helper'

describe 'JobOffersController' do
  describe 'get :new' do
    it 'should response ok and render job_offers/new' do
      get '/job_offers/new'
      expect(last_response).to be_ok
    end

    it 'should render job_offers/new' do
      expect_any_instance_of(JobVacancy::App).to receive(:render).with('job_offers/new')
      get '/job_offers/new'
    end
  end
end
