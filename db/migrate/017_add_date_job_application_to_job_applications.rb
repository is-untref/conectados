Sequel.migration do
  up do
    add_column :job_applications, :date_job_application, DateTime
  end

  down do
    drop_column :job_applications, :date_job_application
  end
end
