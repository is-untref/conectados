Sequel.migration do
  up do
    add_column :users, :last_login_date, DateTime
  end

  down do
    drop_column :users, :last_login_date
  end
end
