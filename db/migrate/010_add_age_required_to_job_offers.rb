Sequel.migration do
  up do
    add_column :job_offers, :age_required, Integer
  end

  down do
    drop_column :job_offers, :age_required
  end
end
