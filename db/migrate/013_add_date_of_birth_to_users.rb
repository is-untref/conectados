Sequel.migration do
  up do
    add_column :users, :date_birth, Date
  end

  down do
    drop_column :users, :date_birth
  end
end
