Sequel.migration do
  up do
    create_table(:job_applications) do
      primary_key :id
      String :applicant_email
      String :description_postulant
    end
  end

  down do
    drop_table(:job_applications)
  end
end
