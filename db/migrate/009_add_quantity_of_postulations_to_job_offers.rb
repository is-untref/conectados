Sequel.migration do
  up do
    add_column :job_offers, :quantity_of_postulations, Integer
  end

  down do
    drop_column :job_offers, :quantity_of_postulations
  end
end
