Sequel.migration do
  up do
    add_column :job_applications, :job_offer_id, Integer
  end

  down do
    drop_column :job_applications, :job_offer_id
  end
end
