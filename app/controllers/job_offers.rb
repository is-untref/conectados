require 'recaptcha'

JobVacancy::App.controllers :job_offers do
  include Recaptcha::ClientHelper
  include Recaptcha::Verify

  Recaptcha.configure do |config|
    config.site_key = ENV['CAPTCHA_SITE']
    config.secret_key = ENV['CAPTCHA_SECRET']
  end

  get :my do
    @offers = JobOffer.find_by_owner(current_user)
    render 'job_offers/my_offers'
  end

  get :index do
    @offers = JobOffer.all_active
    render 'job_offers/list'
  end

  get :new do
    @job_offer = JobOffer.new
    render 'job_offers/new'
  end

  get :latest do
    @offers = JobOffer.all_active
    render 'job_offers/list'
  end

  get :edit, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    # TODO: validate the current user is the owner of the offer
    render 'job_offers/edit'
  end

  get :apply, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    @job_application = JobApplication.new
    render 'job_offers/apply'
  end

  get :question, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    @job_application = JobQuestion.new
    render 'job_offers/question'
  end

  get :applications, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    @applications = JobApplication.find_by_job_offer(@job_offer)
    render 'job_offers/applications'
  end

  post :search do
    b = "%#{params[:q]}%"
    # rubocop:disable LineLength
    @offers = JobOffer.where(Sequel.ilike(:title, b)).or(Sequel.ilike(:description, b)).or(Sequel.ilike(:location, b)).or(Sequel.ilike(:tags, b))
    # rubocop:enable LineLength
    render 'job_offers/list'
  end

  # rubocop:disable BlockLength
  post :apply, with: :offer_id do
    if verify_recaptcha(@job_application)
      @job_offer = JobOffer.with_pk(params[:offer_id])
      applicant_email = params[:job_application][:applicant_email]
      description_postulant = params[:job_application][:description_postulant]
      result = validate_email_for_application(applicant_email)
      if result == ''
        @loggued_user = current_user
        date = @loggued_user.date_birth
        num_date = date.strftime('%Y-%m-%d')
        format_date = Date.strptime(num_date, '%Y-%m-%d')
        str_date = format_date.strftime('%d-%m-%Y')
        @job_application = JobApplication.create_for(applicant_email, @job_offer, description_postulant, str_date)
        @job_offer.add_postulant(params[:offer_id])
        @job_application.values[:applicant_email] = applicant_email
        @job_application.values[:description_postulant] = description_postulant
        @job_application.values[:job_offer_id] = params[:offer_id]
        @job_application.values[:date_job_application] = Time.zone.now.strftime('%Y-%m-%d %H:%M:%S')
        @job_application.save
        @job_application.process
        flash[:success] = 'Contact information sent. Job Application information saved.'
        redirect '/job_offers'
      else
        flash[:error] = result
        redirect "/job_offers/apply/#{params[:offer_id]}"
      end
    else
      flash[:error] = 'Please fill in the captcha.'
      redirect "/job_offers/apply/#{params[:offer_id]}"
    end
  end
  # rubocop:enable BlockLength

  post :question, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    user_email = params[:job_question][:email]
    subject = params[:job_question][:subject]
    question = params[:job_question][:question]
    @job_application = JobQuestion.create_for_question(user_email, @job_offer, subject, question)
    if validate_params(user_email, subject, question)
      @job_application.send_question
      flash[:success] = 'Question information sent.'
      redirect '/job_offers'
    else
      flash.now[:error] = 'All fields are mandatory'
      render 'job_offers/question'
    end
  end

  post :create do
    analyzer = AnalyzerOfRemuneration.new
    analyzer_age = AnalyzerOfAgeRequired.new
    begin
      analyzer.analyze_remuneration(params[:job_offer][:remuneration])
      analyzer_age.analyze_age_required(params[:job_offer][:age_required])
    rescue RuntimeError => exception
      flash[:error] = exception.message
      redirect 'job_offers/new'
    end
    age = params[:job_offer][:age_required]
    params[:job_offer][:age_required] = 0 if age == ''
    tags = params[:job_offer][:tags].to_s
    tags_downcase = convert_tags(tags).downcase
    @job_offer = JobOffer.new(params[:job_offer])
    @job_offer.values[:tags] = tags_downcase
    @job_offer.owner = current_user

    if @job_offer.save
      TwitterClient.publish(@job_offer) if params['create_and_twit']
      flash[:success] = 'Offer created'
      redirect '/job_offers/my'
    else
      flash.now[:error] = 'Title is mandatory'
      render 'job_offers/new'
    end
  end

  post :update, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    tags = params[:job_offer][:tags].to_s
    tags_downcase = convert_tags(tags).downcase
    @job_offer.update(params[:job_offer])
    @job_offer.values[:tags] = tags_downcase
    if @job_offer.save
      analyzer_age = AnalyzerOfAgeRequired.new
      begin
        analyzer_age.analyze_age_required(params[:job_offer][:age_required])
      rescue RuntimeError => exception
        flash[:error] = exception.message
        redirect "/job_offers/edit/#{params[:offer_id]}"
      end
      flash[:success] = 'Offer updated'
      redirect '/job_offers/my'
    else
      flash.now[:error] = 'Title is mandatory'
      render 'job_offers/edit'
    end
  end

  put :activate, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    @job_offer.activate
    if @job_offer.save
      flash[:success] = 'Offer activated'
    else
      flash.now[:error] = 'Operation failed'
    end

    redirect '/job_offers/my'
  end

  delete :destroy do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    if !@job_offer.quantity_of_postulations.nil?
      flash[:error] = 'Offer has a postulation and cannot be erased'
      redirect 'job_offers/my'
    elsif @job_offer.destroy
      flash[:success] = 'Offer deleted'
      redirect 'job_offers/my'
    else
      flash.now[:error] = 'Title is mandatory'
      redirect '/'
    end
  end
end
