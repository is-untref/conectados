JobVacancy::App.controllers :sessions do
  get :login, map: '/login' do
    @user = User.new
    render 'sessions/new'
  end

  post :create do
    email = params[:user][:email]
    @user = User.authenticate(email, params[:user][:password])
    failed_logins = User.where(email: email).get(:failed_logins)
    if is_blocked?(email) # User blocked
      redirect_blocked
    elsif !@user.nil? # User authenticated
      case_user_authenticated failed_logins
    elsif !failed_logins.nil? # User exist and bad authenticated
      failed_logins += 1
      User.where(email: email).update(failed_logins: failed_logins)
      case_user_not_authenticated_and_exists(failed_logins, email)
    else
      redirect_invalid_credentials
    end
  end

  get :destroy, map: '/logout' do
    sign_out
    redirect '/'
  end
end
