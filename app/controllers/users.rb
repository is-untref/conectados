require_relative '../../models/analyzer_of_password'
require_relative '../../models/analyzer_of_date_birth'
require 'securerandom'

JobVacancy::App.controllers :users do
  get :new, map: '/register' do
    @user = User.new
    render 'users/new'
  end
  # rubocop:disable BlockLength
  post :create do
    analyzer = AnalyzerOfPassword.new
    date_analyzer = AnalyzerOfDateBirth.new
    password_confirmation = params[:user][:password_confirmation]
    params[:user].reject! { |k, _v| k == 'password_confirmation' }

    if params[:user][:date_birth] == ''
      flash.now[:error] = 'All fields are mandatory'
      @user = User.new
      render 'users/new'
    else
      begin
        analyzer.analyze_size(params[:user][:password])
        analyzer.analyze_numbers(params[:user][:password])
        analyzer.compare_passwords(params[:user][:password], password_confirmation)
        date_analyzer.date_validate(params[:user][:date_birth])
      rescue RuntimeError => exception
        flash[:error] = exception.message
        redirect 'register'
      end

      @user = User.new(params[:user])
      if @user.save
        flash[:success] = 'User created'
        redirect '/'
      else
        flash.now[:error] = 'All fields are mandatory'
        render 'users/new'
      end
    end
  end
  # rubocop:enable BlockLength

  get :forgot do
    @user = User.new
    render 'forgotten_password'
  end

  post :forgot do
    user = User.first(email: params[:user][:email])
    if user.nil?
      flash.now[:error] = 'User does not exist'
      @user = User.new
      render 'forgotten_password'
    else
      new_pass = SecureRandom.urlsafe_base64(10, true)
      user.password = new_pass
      user.save
      JobVacancy::App.deliver(:notification, :forgotten_password, params[:user][:email], new_pass)
      flash[:success] = 'new password was sent to your email'
      redirect '/'
    end
  end
end
