# Helper methods defined here can be accessed in any controller or view in the application
# rubocop:disable BlockLength
JobVacancy::App.helpers do
  attr_writer :current_user
  def validate_params(email, subject, question)
    if email != '' && subject != '' && question != ''
      true
    else
      false
    end
  end

  def validate_email_for_application(email)
    job_application = JobApplication.new
    result = ''
    result = 'Format email invalid.' unless job_application.validate_email(email)
    result = 'An email is required' if email == ''
    result
  end

  def current_user
    @current_user ||= User.with_pk(session[:current_user])
  end

  def date_birth
    @current_user.date_birth
  end

  def signed_in?
    !current_user.nil?
  end

  def convert_tags(tags)
    tags_separated = tags.split(',')
    tags_separated.delete_if { |tag| tag == ' ' }
    tags_joined = tags_separated.join(',')
    tags_joined
  end

  def related_offers_to(tags)
    tags_separated = tags.split(',')
    offers_related = JobOffer.where(Sequel.ilike(:tags, "%#{tags_separated[0]}%"))
    # rubocop:disable For
    for i in 1..(tags_separated.size - 1)
      # rubocop:disable LineLength
      offers_related = offers_related.union(JobOffer.where(Sequel.like(:tags, "%#{tags_separated[i]}%")))
      # rubocop:enable LineLength
    end
    # rubocop:enable For
    offers_related
  end
end
# rubocop:enable BlockLength
