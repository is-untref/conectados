# Helper methods defined here can be accessed in any controller or view in the application

JobVacancy::App.helpers do
  attr_writer :current_user
  def current_user
    @current_user ||= User.with_pk(session[:current_user])
  end

  def date_birth
    current_user.date_birth
  end
  # def simple_helper_method
  #  ...
  # end
end
