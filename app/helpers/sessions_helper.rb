# rubocop:disable BlockLength
JobVacancy::App.helpers do
  attr_writer :current_user

  def current_user
    @current_user ||= User.with_pk(session[:current_user])
  end

  def sign_in(user)
    session[:current_user] = user.id
    User.where(email: user.email).update(failed_logins: 0)
    User.where(email: user.email).update(last_login_date: DateTime.now)
    self.current_user = user
  end

  def sign_out
    session.delete(:current_user)
  end

  def signed_in?
    !current_user.nil?
  end

  def date_birth
    current_user.date_birth
  end

  def redirect_recaptcha
    @user = User.new
    flash.now[:error] = 'more than 3 times bad password, please fill in the captcha'
    render 'sessions/new_with_captcha'
  end

  def redirect_invalid_credentials
    @user = User.new
    flash.now[:error] = 'Invalid credentials'
    render 'sessions/new'
  end

  def redirect_blocked
    @user = User.new
    flash[:error] = 'more than 6 times bad password, user is blocked 24hs'
    redirect '/'
  end

  # assumes not blocked
  def case_user_authenticated(failed_logins)
    case failed_logins
    when 0..2
      sign_in @user
      redirect '/'
    when 3..99
      if verify_recaptcha(@user)
        sign_in @user
        redirect '/'
      else
        redirect_recaptcha
      end
    end
  end

  def case_user_not_authenticated_and_exists(failed_logins, user_email)
    case failed_logins
    when 0..2
      redirect_invalid_credentials
    when 3..5
      redirect_recaptcha
    when 6
      User.where(email: user_email).update(last_login_date: DateTime.now)
      redirect_blocked
    when 7..99
      redirect_blocked
    end
  end

  def is_blocked?(email)
    last_login = User.where(email: email).get(:last_login_date)
    return false if last_login.nil?

    if User.where(email: email).get(:failed_logins) > 5
      time_since_lock = ((Time.parse(DateTime.now.to_s) - Time.parse(last_login.to_s)) / 3600)
      time_since_lock < 24
    else
      false
    end
  end
end
# rubocop:enable BlockLength
