module JobVacancy
  class App < Padrino::Application
    register Padrino::Rendering
    register Padrino::Mailer
    register Padrino::Helpers

    enable :sessions

    if ENV['SENDGRID_ADDRESS'].nil?
      set :delivery_method, file: {
        location: "#{Padrino.root}/tmp/emails"
      }
    else
      set :delivery_method, smtp: {
        address: ENV['SENDGRID_ADDRESS'],
        port: ENV['SENDGRID_PORT'],
        user_name: ENV['SENDGRID_USERNAME'],
        password: ENV['SENDGRID_PASSWORD'],
        authentication: :plain,
        domain: 'localhost.localdomain'
      }
    end
  end
end
