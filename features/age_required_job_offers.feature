Feature: Age Required

Scenario: When I create a new job’s offer and I put a age required it should show it when the job’s offer is created.
  Given I’m logued like offerer
  When I want to create a new job offer 
  And I put the title "PHP" 
  And I put the age required 25
  Then I should see the Offer created message
  And it show in the age required field 25

Scenario: When I create a new job’s offer and I don’t put a age required it should show a blank space when the job’s offer is created.
  Given I’m logued like offerer
  When I want to create a new job offer
  And I put the title "PHP" 
  And I don’t put the age required
  Then I should see the Offer created message
  And it should show “Not specified” in the age required’s field

Scenario: Update age required when the job’s offer is created.
  Given I’m logued like offerer
  And a job offer is created with the name "PHP" and the age required 25
  When I see my job offers
  And I edit it 
  And I set the age required to 30
  Then I should see the Offer created message
  And it show in the age required field 30