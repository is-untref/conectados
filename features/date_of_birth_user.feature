Feature: Day of Birth

Scenario: Does not register successfully without birth day
  Given access the register page 
  And ingress name "Luciana" 
  And ingress email "luciana@gmail.com" 
  And ingress password "password1234" with confirmation password "password1234"
  When the user do “Create”
  Then the user doesn’t register and show a message’s error "All fields are mandatory"

Scenario: Registration succesfull with day birth
  Given access the register page 
  And ingress name "Luciana" 
  And ingress email "luciana@gmail.com" 
  And ingress date of birth "1990-05-06" 
  And ingress password "password1234" with confirmation password "password1234"
  When the user do “Create”
  Then the user registers succesfully

Scenario: Registration unsuccesfull because user too old
  Given access the register page 
  And ingress name "Luciana" 
  And ingress email "luciana@gmail.com" 
  And ingress date of birth "1900-05-06" 
  And ingress password "password1234" with confirmation password "password1234"
  When the user do “Create”
  Then the user doesn’t register and show a message’s error "Must be over 18 years old and under 65 years old."

Scenario: Registration unsuccesfull because user too young
  Given access the register page 
  And ingress name "Luciana" 
  And ingress email "luciana@gmail.com" 
  And ingress date of birth "2010-05-06" 
  And ingress password "password1234" with confirmation password "password1234"
  When the user do “Create”
  Then the user doesn’t register and show a message’s error "Must be over 18 years old and under 65 years old."  
