Feature: User login blocked

Scenario: When a user login and he failed his password 6 times, user is blocked

  Given the existant of a user with mail "maxi@test.com" and password "password1234"
  And the user "maxi@test.com" failed his password 6 times
  And the users put his password "wrongly"
  When he tries to login
  Then the login is "unsuccessful"
  And the user should see the "more than 6 times bad password, user is blocked 24hs" message

Scenario: When was blocked 24hs ago, it is unblocked

  Given the existant of a user with mail "maxi@test.com" and password "password1234"
  And the user "maxi@test.com" failed his password 8 times
  And the users put his password "correctly"
  And more than 1 day have passed since last login
  When he tries to login
  Then the login is "successful"
