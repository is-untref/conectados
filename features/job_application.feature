Feature: Job Application
  In order to get a job
  As a candidate
  I want to apply to an offer

  Background:
  	Given only a "Web Programmer" offer exists in the offers list
    And Im registered and logged in as "juan@test.com"
    And I access the offers list page

  Scenario: When a user writes his bio, it is sent to the offerrer 
    Given I apply a job with the applicant email field with "maxibiscardi@gmail.com"
    And I fill the personal description field  with "Buen dia, me gustaria trabajar en su empresa"
    When the user "does" complete the captcha and apply
    Then An email is sent to the offerrer and includes "Buen dia, me gustaria trabajar en su empresa"

  Scenario: When a user leave the his bio empty, it is sent to the offerrer without it
    Given I apply a job with the applicant email field with "maxibiscardi@gmail.com"
    And I fill the personal description field with nothing
    When the user "does" complete the captcha and apply
    Then An email is sent to the offerrer and shows short bio as empty

  Scenario: When a user applied in a job’s offer without putting the email it should not save in the database.

    When the user wants to apply in that job offer 
    And the users put the email ""
    And the user puts the description "Quisiera ser parte de su empresa"
    Then the job offer is not saved 
    And the user should see the "An email is required" message

  Scenario: When a user can not apply in a job if they are not logged in

    Given the user has not logged in
    When the user accesses the list of offers page
    Then the user should see the "You must be logged in to be able to apply for an offer" message
