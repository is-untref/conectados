Feature: Job Applications of a job offer

Background:
  Given Im registered and logged in as "juan@test.com"
  Given I have a job’s offer of "Ruby programmer" published

Scenario: When 2 people applied to a job’s offer, the offerer can see the information about two postulations.
  Given A user with his email "agustina@test.com"  applied in that job
  And A user with his email "lucas@test.com"  applied in that job
  When I see my job’s offers
  And I want to see the postulations of that job
  Then I should see 2 postulations and an email "agustina@test.com".

Scenario: When 0 people applied to a job’s offer, the offerer will see none of postulations about that job offer.
  Given 0 people applied in that job
  When I see my job’s offers
  And I want to see the postulations of that job
  Then I should see the message "The job offer has no applications yet"
