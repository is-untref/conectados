Feature: Suggested Job Offer

Background:
  Given Im registered and logged in as "juan@test.com"
  And I access the offers list page    

Scenario: Job Offer with one tag
  Given "Web Programmer" offer exists in the offers list with tag "web, programmer"
  When the user applies to the offer
  Then the user should see “Other jobs offers that may interest you..”

Scenario: Job Offer without tag
  Given "PHP Programmer" offer exists in the offers list without tag
  When the user applies to the offer
  Then the user should not see “Other jobs offers that may interest you..”
