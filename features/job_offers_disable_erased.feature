Feature: Story # 42:  disble delete in Job offers

Background:
  Given Im registered and logged in as "juantopo@test.com"

Scenario: When nobody applied to the job, it can be erased
  Given I have a job’s offer of "Java programmer" published
  And 0 people applied in that job
  And I see my job’s offers
  When I delete it
  Then I should see "Offer deleted"

Scenario: When someone already applied to the job, it can not be erased
  Given I have a job’s offer of "Ruby programmer" published
  And 2 people applied in that job
  And I see my job’s offers
  When I delete it
  Then I should see "Offer has a postulation and cannot be erased"
