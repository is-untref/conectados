Feature: Search in all fields

Background:
  Given only a "Web Programmer" offer exists in the offers list, with location "caseros" and description "Ruby programmer with rails knwoledge is searched in caseros"


Scenario: When a user search, search is done in title field

  Given a user looking at offers list
  When the user search  "ruby"
  Then the job offer for "Ruby programmer" is displayed

Scenario: When a user search, search is done in location field

  Given a user looking at offers list
  When the user search  "caser"
  Then the job offer for "Ruby programmer" is displayed

Scenario: When a user search, search is done in description field

  Given a user looking at offers list
  When the user search  "Rails"
  Then the job offer for "Ruby programmer" is displayed
