Feature: User login captcha

  Background:
    Given there is no one logged in
    And Im registered as "maxibiscardi@gmail.com"

Scenario: When a user login and he failed his password 3 times

  Given the user visit de login page
  And the user puts your mail "maxibiscardi@gmail.com"
  When the user "maxibiscardi@gmail.com" failed his password 4 times
  When the users put his password "wrongly"
  And he tries to login
  Then he is redirected to login screen with captcha displayed

Scenario: When a user login and he never failed its password
  
  Given the user visit de login page
  And the user puts your mail "maxibiscardi@gmail.com"
  When the users put his password "correctly"
  And he tries to login
  Then the login is "successful"
