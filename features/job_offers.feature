Feature: Job Offers

Background:
    Given Im registered and logged in as "juan@test.com"

Scenario: When 2 people applied to a job’s offer, it shows the number in “Number of postulations”’s field.
  Given I have a job’s offer of "ruby programmer" published
  And 2 people applied in that job
  When I see my job’s offers
  Then I should see a "2" in "# applications"’s field.

Scenario: When 0 people applied to a job’s offer, it shows a middle script in “Number of postulations”’s field.  
  Given I have a job’s offer of "ruby programmer" published
  And 0 people applied in that job
  When I see my job’s offers
  Then I should see a "-" in "# applications"’s field.
