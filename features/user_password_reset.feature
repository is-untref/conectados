Feature: Password reset

Scenario: A registered user resets his password successfully

  Given the existant of a user with mail "maxi@test.com" and password "password1234"
  And the user goes to login screen, and then to Forgot your password?
  And ingress email "maxi@test.com" 
  When he does click on Send new password
  Then the user should see the "new password was sent to your email" message
  And a new password is sent to "maxi@test.com"

Scenario: An unregistered user tries to reset a password

  Given  the user goes to login screen, and then to Forgot your password?
  And ingress email "notRegisteredUser@test.com" 
  When he does click on Send new password
  Then the user should see the "User does not exist" message