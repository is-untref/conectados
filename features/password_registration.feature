Feature: Password registration

Scenario: When the user puts a strong password that include four numbers and a size longer than 10, he registers succesfully.
  Given access the register page 
  And ingress name "Mariana" 
  And ingress email "mariana@gmail.com" 
  And ingress date of birth "1990-05-06"
  And ingress password "password1234" with confirmation password "password1234"
  When the user do “Create”
  Then the user registers succesfully

Scenario: When the user puts a weak password with a size less than 10, he doesn’t register succesfully and shows a message’s error.
  Given access the register page 
  And ingress name "Carla" 
  And ingress email "carla@gmail.com"
  And ingress date of birth "1990-05-06"
  And ingress password "PassDebil" with confirmation password "PassDebil"
  When the user do “Create”
  Then the user doesn’t register and show a message’s error "Password invalid. It should have a size longer than 10."

Scenario: When the user puts a password with a size longer than 10 but not the 4 numbers, he doesn’t register succesfully and shows a message’s error.
  Given access the register page 
  And ingress name "Ludmila" 
  And ingress email "ludmila@gmail.com"
  And ingress date of birth "1990-05-06"
  And ingress password "Password56" with confirmation password "Password56"
  When the user do “Create”
  Then the user doesn’t register and show a message’s error "Password invalid. It should have at least 4 numbers."
