When(/^I browse the default page$/) do
  visit '/'
end

Given(/^I am logged in as job offerer$/) do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  page.should have_content('offerer@test.com')
end

Given(/^I access the new offer page$/) do
  visit '/job_offers/new'
  page.should have_content('Title')
end

When(/^I fill the title with "(.*?)"$/) do |offer_title|
  fill_in('job_offer[title]', with: offer_title)
  fill_in('job_offer[age_required]', with: 21)
end

When(/^confirm the new offer$/) do
  click_button('Create')
end

Then(/^I should see "(.*?)" in My Offers$/) do |content|
  visit '/job_offers/my'
  page.should have_content(content)
end

Then(/^I should not see "(.*?)" in My Offers$/) do |content|
  visit '/job_offers/my'
  page.should_not have_content(content)
end

Given(/^I have "(.*?)" offer in My Offers$/) do |offer_title|
  JobOffer.all.each(&:delete)
  visit '/job_offers/new'
  fill_in('job_offer[title]', with: offer_title)
  fill_in('job_offer[age_required]', with: 21)
  click_button('Create')
end

Given(/^I edit it$/) do
  click_link('Edit')
end

And(/^I delete it$/) do
  click_button('Delete', match: :first)
end

Given(/^I set title to "(.*?)"$/) do |new_title|
  fill_in('job_offer[title]', with: new_title)
end

Given(/^I save the modification$/) do
  click_button('Save')
end

When('I fill the location with {string}') do |location|
  fill_in('job_offer[location]', with: location)
end

When('I fill the age required with {int}') do |age_required|
  fill_in('job_offer[age_required]', with: age_required)
end

When('I fill the remuneration with {int}') do |remuneration|
  fill_in('job_offer[remuneration]', with: remuneration)
end

When('I fill the tags with {string}') do |tags|
  fill_in('job_offer[tags]', with: tags)
end

Then('It should show {string} in the column {string} in My Offers') do |tags_saved, column|
  page.should have_content(column)
  page.should have_content(tags_saved)
end
