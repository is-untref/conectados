Given('a job offer is created with the name {string} and the age required {int}') do |title, age_required|
  JobOffer.all.each(&:delete)
  visit '/job_offers/new'
  fill_in('job_offer[title]', with: title)
  fill_in('job_offer[age_required]', with: age_required)
  click_button('Create')
end

When('I put the age required {int}') do |age_required|
  fill_in('job_offer[age_required]', with: age_required)
end

When('I don’t put the age required') do
  fill_in('job_offer[age_required]', with: 0)
end

When('I set the age required to {int}') do |age_required|
  fill_in('job_offer[age_required]', with: age_required)
end

Then('it show in the age required field {int}') do |age_required|
  expect(find_field('job_offer[age_required]').value.to_i).to eq age_required
end

Then('it should show “Not specified” in the age required’s field') do
  expect(find_field('job_offer[age_required]').value.to_i).to eq 0
end
