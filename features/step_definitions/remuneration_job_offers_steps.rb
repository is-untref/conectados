When('I want to create a new job offer') do
  visit '/job_offers/new'
end

When('I put the title {string}') do |title|
  fill_in('job_offer[title]', with: title)
end

When('I don’t put the remuneration') do
  fill_in('job_offer[remuneration]', with: '')
end

When('I put the remuneration {int}') do |remuneration|
  fill_in('job_offer[remuneration]', with: remuneration)
end

Then('I should see the Offer created message') do
  page.has_content? 'Offer Created'
end

Then('it should show {int} in the remuneration’s field') do |remuneration|
  fill_in('job_offer[remuneration]', with: remuneration)
end

Then('it should show “ ” in the remuneration’s field') do
  fill_in('job_offer[remuneration]', with: '')
end

Given('a job offer is created with the name {string} and the remuneration {int}') do |job_offer_name, remuneration|
  JobOffer.all.each(&:delete)
  visit '/job_offers/new'
  fill_in('job_offer[title]', with: job_offer_name)
  fill_in('job_offer[remuneration]', with: remuneration)
  fill_in('job_offer[age_required]', with: 21)
  click_button('Create')
end

When('I see my job offers') do
  visit '/job_offers/my'
end

When('I set the remuneration to {int}') do |new_remuneration|
  fill_in('job_offer[remuneration]', with: new_remuneration)
  click_button('Save')
end

Then('I should show {int} in the remuneration’s field') do |remuneration_updated|
  visit '/job_offers/my'
  page.should have_content(remuneration_updated)
end
