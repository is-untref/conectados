Given('A user with his email {string}  applied in that job') do |email_applicant|
  visit '/job_offers'
  click_link('Apply', match: :first)
  fill_in('job_application[applicant_email]', with: email_applicant)
  fill_in('job_application[description_postulant]', with: 'Tengo amplia experiencia.')
  old_site_key = Recaptcha.configuration.site_key
  old_secret_key = Recaptcha.configuration.secret_key
  Recaptcha.configuration.site_key = '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
  Recaptcha.configuration.secret_key = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'
  click_button('Apply')
  Recaptcha.configuration.site_key = old_site_key
  Recaptcha.configuration.secret_key = old_secret_key
end

When('I want to see the postulations of that job') do
  click_link('Applications', match: :first)
end

Then('I should see {int} postulations and an email {string}.') do |_postulations, email_for_an_applicant|
  @last_page.should have_content(email_for_an_applicant)
end

Then('I should see the message {string}') do |message_no_postulations|
  page.should have_content(message_no_postulations)
end
