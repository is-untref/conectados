Given('the user goes to login screen, and then to Forgot your password?') do
  visit '/users/forgot'
end

When('he does click on Send new password') do
  click_button('Send new password')
end

Then('a new password is sent to {string}') do |user_resetted_email|
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/#{user_resetted_email}", 'r')
  content = file.read
  content.include?('Your password has been resetted, the new one is:').should be true
  file.close
  File.delete("#{mail_store}/#{user_resetted_email}")
end
