Given('access the register page') do
  # page.should_not have_content('luciana@gmail.com')
  visit '/register'
end

Given('ingress name {string}') do |name|
  fill_in('user[name]', with: name)
end

Given('ingress email {string}') do |email|
  fill_in('user[email]', with: email)
end

Given('ingress date of birth {string}') do |date|
  fill_in('user[date_birth]', with: date)
end

Given('ingress password {string} with confirmation password {string}') do |pass, confirmation_pass|
  fill_in('user[password]', with: pass)
  fill_in('user[password_confirmation]', with: confirmation_pass)
end

When('the user do “Create”') do
  click_button('Create')
end
