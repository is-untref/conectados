Given('I apply a job with the applicant email as {string}') do |mail|
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  visit '/job_offers'
  click_link('Apply', match: :first)
  fill_in('job_application[applicant_email]', with: mail)
end

Then('job application {string} created') do |accion|
  mail_store = "#{Padrino.root}/tmp/emails"
  case accion
  when 'is'
    file = File.open("#{mail_store}/#{@applicant_email}", 'r')
    content = file.read
    content.include?(@job_offer.title).should be true
    content.include?(@job_offer.location).should be true
    content.include?(@job_offer.description).should be true
    content.include?(@job_offer.remuneration.to_s).should be true
    content.include?(@job_offer.age_required.to_s).should be true
    content.include?(@job_offer.owner.email).should be true
    content.include?(@job_offer.owner.name).should be true
    content.include?(@job_offer.owner.name).should be true
    file.close
    File.delete("#{mail_store}/#{@applicant_email}")
  when 'is not'
    !File.file?("#{mail_store}/#{@applicant_email}")
  else
    false
  end
end

When('the user {string} complete the captcha and apply') do |accion|
  old_site_key = Recaptcha.configuration.site_key
  old_secret_key = Recaptcha.configuration.secret_key

  case accion
  when 'wrongly'
    click_button('Apply')
    # TODO, search method to complete the captcha wrong
  when 'does'
    Recaptcha.configuration.site_key = '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
    Recaptcha.configuration.secret_key = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'
    click_button('Apply')
    Recaptcha.configuration.site_key = old_site_key
    Recaptcha.configuration.secret_key = old_secret_key
  when 'does not'
    click_button('Apply')
  else
    false
  end
end
