Given('{string} offer exists in the offers list with tag {string}') do |job_title, tag|
  JobOffer.all.each(&:delete)
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = job_title
  @job_offer.location = 'Caseros'
  @job_offer.description = 'A nice job'
  @job_offer.age_required = 21
  @job_offer.remuneration = 21_000
  @job_offer.tags = tag
  @job_offer.save
end

When('the user applies to the offer') do
  visit '/job_offers'
  click_link('Apply')
end

Then('the user should see “Other jobs offers that may interest you..”') do
  page.should have_content('Other jobs offers that may interest you..')
end

Given('{string} offer exists in the offers list without tag') do |job_title|
  JobOffer.all.each(&:delete)
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = job_title
  @job_offer.location = 'Caseros'
  @job_offer.description = 'A nice job'
  @job_offer.age_required = 21
  @job_offer.remuneration = 21_000
  @job_offer.save
end

Then('the user should not see “Other jobs offers that may interest you..”') do
  page.should_not have_content('Other jobs offers that may interest you..')
end
