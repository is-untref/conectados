When('the user puts the password {string}') do |password|
  visit '/register'
  fill_in('user[name]', with: 'Juan')
  fill_in('user[email]', with: 'juan@test.com')
  fill_in('user[password]', with: password)
  fill_in('user[date_birth]', with: Date.new(1992, 11, 11))
  fill_in('user[password_confirmation]', with: password)
  click_button('Create')
end

Then('the user registers succesfully') do
  page.should have_content('User created')
end

Then('the user doesn’t register and show a message’s error {string}') do |error_message|
  page.should have_content(error_message)
end
