Given('I’m logued like offerer') do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  page.should have_content('offerer@test.com')
end

Given('I have a job’s offer of {string} published') do |job_offer_published|
  JobOffer.all.each(&:delete)
  visit '/job_offers/new'
  fill_in('job_offer[title]', with: job_offer_published)
  fill_in('job_offer[location]', with: 'CABA')
  fill_in('job_offer[age_required]', with: 21)
  fill_in('job_offer[description]', with: 'Ruby Programer for important company')
  click_button('Create')
end

Given('{int} people applied in that job') do |number_postulations|
  visit '/job_offers'
  i = 0
  while i < number_postulations
    click_link('Apply', match: :first)
    fill_in('job_application[applicant_email]', with: 'postulant@test.com')
    click_button('Apply')
    i += 1
    visit '/job_offers'
  end
end

When('I see my job’s offers') do
  visit '/job_offers/my'
  @last_page = page
end

Then('I should see a {string} in {string}’s field.') do |content, column|
  @last_page.should have_content(content)
  @last_page.should have_content(column)
end

Then('I should see a {int} in {string}’s field.') do |number_postulations, column|
  @last_page.should have_content(number_postulations)
  @last_page.should have_content(column)
end
