Given('only a {string} offer exists in the offers list, with location {string} and description {string}') do |job_title, location, description|
  JobOffer.all.each(&:delete)
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = job_title
  @job_offer.location = location
  @job_offer.description = description
  @job_offer.age_required = 21
  @job_offer.remuneration = 21_000
  @job_offer.save
end

Given('a user looking at offers list') do
  visit '/job_offers/latest'
end

When('the user search  {string}') do |search_criteria|
  fill_in('q', with: search_criteria)
  click_button('search-button')
end

Then('the job offer for {string} is displayed') do |expected_result|
  page.should have_content(expected_result)
end
