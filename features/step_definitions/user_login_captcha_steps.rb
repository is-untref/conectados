Given('the existant of a user with mail {string} and password {string}') do |mail, password|
  @mail = mail
  @pass = password
  visit '/register'
  fill_in('user[name]', with: mail)
  fill_in('user[email]', with: mail)
  fill_in('user[date_birth]', with: Date.new(1992, 11, 11))
  fill_in('user[password]', with: password)
  fill_in('user[password_confirmation]', with: password)
  click_button('Create')
end

When('the user {string} failed his password {int} times') do |mail, cantidad_fallas|
  User.where(email: mail).update(failed_logins: 0)
  (1..cantidad_fallas).each do
    visit '/login'
    fill_in('user[email]', with: mail)
    fill_in('user[password]', with: 'passinvalida9876')
    click_button('Login')
  end
end

When('the users put his password {string}') do |accion|
  case accion
  when 'correctly'
    visit '/login'
    fill_in('user[email]', with: @mail)
    fill_in('user[password]', with: @pass)
  when 'wrongly'
    visit '/login'
    fill_in('user[email]', with: @mail)
    fill_in('user[password]', with: '123')
  else
    false
  end
end

When('he tries to login') do
  click_button('Login')
end

Then('the login is {string}') do |accion|
  case accion
  when 'successful'
    page.should have_content(@mail)
  when 'unsuccessful'
    page.should_not have_content(@mail)
  else
    false
  end
  User.where(email: @mail).delete
end

Then('he is redirected to login screen with captcha displayed') do
  current_url.should eq('http://www.example.com/sessions/create')
end

Given('there is no one logged in') do
  visit '/logout'
end

Given('the user visit de login page') do
  visit '/login'
end

Given('the user puts your mail {string}') do |mail|
  fill_in('user[email]', with: mail)
end

Given('Im registered as {string}') do |mail|
  visit '/register'
  fill_in('user[name]', with: mail)
  fill_in('user[email]', with: mail)
  fill_in('user[date_birth]', with: Date.new(1992, 12, 11))
  fill_in('user[password]', with: 'password1234')
  fill_in('user[password_confirmation]', with: 'password1234')
  click_button('Create')
end

Given('more than 1 day have passed since last login') do
  User.where(email: @mail).update(last_login_date: (DateTime.now - 2))
end
