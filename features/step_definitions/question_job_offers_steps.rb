Given('the list of the job offers and I choose the {string} job offer') do |job_offer_title|
  JobOffer.all.each(&:delete)
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = job_offer_title
  @job_offer.location = 'Caseros'
  @job_offer.description = 'Ruby programmer for an important company'
  @job_offer.age_required = 20
  @job_offer.remuneration = 17_000
  @job_offer.save
end

When('I want to do a question') do
  visit '/job_offers'
  click_link('Question', match: :first)
end

When('I put the email {string}') do |email|
  fill_in('job_question[email]', with: email)
end

When('I put the subject {string}') do |subject|
  fill_in('job_question[subject]', with: subject)
end

When('I put the question {string}') do |question|
  fill_in('job_question[question]', with: question)
  click_button('Send')
end

Then('it should send an email with that information to the offerer') do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/offerer@test.com", 'r')
  content = file.read
  content.include?(@job_offer.title).should be true
  content.include?(@job_offer.location).should be true
  content.include?(@job_offer.description).should be true
  content.include?(@job_offer.remuneration.to_s).should be true
  content.include?(@job_offer.age_required.to_s).should be true
  content.include?('El trabajo es full time o part time?').should be true
end

Then('it should not send an email to the offerer and it should show a message {string}') do |error|
  page.has_content? error
end
