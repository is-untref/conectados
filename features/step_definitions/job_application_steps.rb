Given(/^only a "(.*?)" offer exists in the offers list$/) do |job_title|
  JobOffer.all.each(&:delete)
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = job_title
  @job_offer.location = 'Caseros'
  @job_offer.description = 'A nice job'
  @job_offer.age_required = 21
  @job_offer.remuneration = 21_000
  @job_offer.save
end

Given(/^I access the offers list page$/) do
  visit '/job_offers'
end

When(/^I apply the offer$/) do
  click_link('Apply', match: :first)
  fill_in('job_application[applicant_email]', with: 'applicant@test.com')
  old_site_key = Recaptcha.configuration.site_key
  old_secret_key = Recaptcha.configuration.secret_key
  Recaptcha.configuration.site_key = '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
  Recaptcha.configuration.secret_key = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'
  click_button('Apply')
  Recaptcha.configuration.site_key = old_site_key
  Recaptcha.configuration.secret_key = old_secret_key
end

Then(/^I should receive a mail with offerer info$/) do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/applicant@test.com", 'r')
  content = file.read
  content.include?(@job_offer.title).should be true
  content.include?(@job_offer.location).should be true
  content.include?(@job_offer.description).should be true
  content.include?(@job_offer.remuneration.to_s).should be true
  content.include?(@job_offer.age_required.to_s).should be true
  content.include?(@job_offer.owner.email).should be true
  content.include?(@job_offer.owner.name).should be true
end

Given('I apply a job with the applicant email field with {string}') do |mail|
  click_link('Apply', match: :first)
  fill_in('job_application[applicant_email]', with: mail)
  @applicant_email = mail
end

Given('I fill the personal description field  with {string}') do |description|
  fill_in('job_application[description_postulant]', with: description)
end

Given('I fill the personal description field with nothing') do
  fill_in('job_application[description_postulant]', with: 'Empty')
end

When('I do in apply') do
  click_button('Apply')
end

Then('An email is sent to the offerrer and includes {string}') do |description|
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/Offerer", 'r')
  content = file.read
  content.include?(description).should be true
end

Then('An email is sent to the offerrer and shows short bio as empty') do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/Offerer", 'r')
  content = file.read
  content.include?('Empty').should be true
end

When('the user wants to apply in that job offer') do
  visit '/job_offers'
  click_link('Apply', match: :first)
end

When('the users put the email {string}') do |mail|
  fill_in('job_application[applicant_email]', with: mail)
end

When('the user puts the description {string}') do |description|
  fill_in('job_application[description_postulant]', with: description)
  old_site_key = Recaptcha.configuration.site_key
  old_secret_key = Recaptcha.configuration.secret_key
  Recaptcha.configuration.site_key = '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
  Recaptcha.configuration.secret_key = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'
  click_button('Apply')
  Recaptcha.configuration.site_key = old_site_key
  Recaptcha.configuration.secret_key = old_secret_key
end

When('the user puts in the description {string}') do |description|
  fill_in('job_application[description_postulant]', with: description)
end

Then('the job offer is saved') do
end

Then('the user should see the {string} message') do |message|
  page.should have_content(message)
end

Then('the job offer is not saved') do
end

Given('Im registered and logged in as {string}') do |mail|
  visit '/register'
  fill_in('user[name]', with: 'user')
  fill_in('user[email]', with: mail)
  fill_in('user[date_birth]', with: Date.new(1992, 12, 11))
  fill_in('user[password]', with: 'password1234')
  fill_in('user[password_confirmation]', with: 'password1234')
  click_button('Create')
  visit '/login'
  fill_in('user[email]', with: mail)
  fill_in('user[password]', with: 'password1234')
  click_button('Login')
end

Given('the user has not logged in') do
  visit '/logout'
end

When('the user accesses the list of offers page') do
  visit '/job_offers'
end
