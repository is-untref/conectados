Feature: Tags in job offers

  Background:
    Given I am logged in as job offerer

  Scenario: One tag in a job offer
    Given I access the new offer page
    When I fill the title with "Ruby Programmer"
    And I fill the location with "San Martin"
    And I fill the age required with 20
    And I fill the remuneration with 20000
    And I fill the tags with "Ruby"
    And confirm the new offer    
    Then I should see "Offer created"
    And It should show "ruby" in the column "Tags" in My Offers

Scenario: Three tags in a job offer
    Given I access the new offer page
    When I fill the title with "Java Programmer"
    And I fill the location with "Caseros"
    And I fill the age required with 20
    And I fill the remuneration with 20000
    And I fill the tags with "JAVA, programmer, Dev"
    And confirm the new offer    
    Then I should see "Offer created"
    And It should show "java, programmer, dev" in the column "Tags" in My Offers

Scenario: No tags in a job offer
    Given I access the new offer page
    When I fill the title with "Java Programmer"
    And I fill the location with "Caseros"
    And I fill the age required with 20
    And I fill the remuneration with 20000
    And I fill the tags with ""
    And confirm the new offer    
    Then I should see "Offer created"
    And It should show "-" in the column "Tags" in My Offers