  Feature: Job Application

  Background:
    Given only a "Web Programmer" offer exists in the offers list
    And Im registered and logged in as "juan@test.com"
    And I access the offers list page

  Scenario: A user applies to a job and does not complete the captcha, failing
    Given I apply a job with the applicant email as "maxibiscardi@gmail.com"
    When the user "does not" complete the captcha and apply
    Then job application "is not" created   

  Scenario: A user applies to a job and complete the captcha wrongly, failing  
    Given I apply a job with the applicant email as "maxibiscardi@gmail.com"
    When the user "wrongly" complete the captcha and apply
    Then job application "is not" created  