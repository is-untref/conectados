Feature: Remuneration in job's offer

Scenario: When I create a new job’s offer and I put a remuneration it should show it when the job’s offer is created.
  Given I’m logued like offerer
  When I want to create a new job offer 
  And I put the title "Phyton Programmer" 
  And I put the remuneration 15000
  Then I should see the Offer created message
  And it should show 15000 in the remuneration’s field

Scenario: When I create a new job’s offer and I don’t put a remuneration it should show a blank space when the job’s offer is created.
  Given I’m logued like offerer
  When I want to create a new job offer
  And I put the title "Phyton Programmer" 
  And I don’t put the remuneration
  Then I should see the Offer created message
  And it should show “ ” in the remuneration’s field

Scenario: Update remuneration when the job’s offer is created.
  Given I’m logued like offerer
  And a job offer is created with the name "Ruby Programmer" and the remuneration 15000
  When I see my job offers
  And I edit it 
  And I set the remuneration to 17000
  Then I should see "Offer updated"
  And I should show 17000 in the remuneration’s field
