Feature: Question about a job offer

Scenario: When I want to apply on a job offer writing a question for that job it should send an email to the offerer with the question.

  Given the list of the job offers and I choose the "Ruby programmer" job offer
  When I want to do a question 
  And I put the email "juan_suarez@hotmail.com" 
  And I put the subject "Modo de contratación"
  And I put the question "El trabajo es full time o part time?"
  Then it should send an email with that information to the offerer


Scenario: When I want to apply on a job offer writing a question for that job without the subject it should not send the email to the offerer .

  Given the list of the job offers and I choose the "Ruby programmer" job offer
  When I want to do a question 
  And I put the email "juan_suarez@hotmail.com" 
  And I put the subject " "
  And I put the question "El trabajo es full time o part time?"
  Then it should not send an email to the offerer and it should show a message "All fields are mandatory"

