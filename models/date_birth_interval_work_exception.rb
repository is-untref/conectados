class DateBirthIntervalWorkException < StandardError
  def initialize
    raise 'Must be over 18 years old and under 65 years old.'
  end
end
