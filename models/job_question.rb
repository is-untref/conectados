class JobQuestion
  attr_accessor :user_email
  attr_accessor :job_offer
  attr_accessor :question
  attr_accessor :subject

  @app = JobQuestion.new

  def self.create_for_question(email, offer, subject, question)
    @app.user_email = email
    @app.job_offer = offer
    @app.subject = subject
    @app.question = question
    @app
  end

  def send_question
    JobVacancy::App.deliver(:notification, :info_mail_question, self)
  end
end
