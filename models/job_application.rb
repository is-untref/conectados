class JobApplication < Sequel::Model
  attr_accessor :applicant_email
  attr_accessor :job_offer
  attr_accessor :description_postulant
  attr_accessor :date

  many_to_one :job_offer

  def validate
    super
    validate_email(:applicant_email)
  end

  def validate_email(email)
    result = email.to_s.match(/\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/)
    !result.nil?
  end

  def self.create_for(email, offer, description_postulant, date)
    app = JobApplication.new
    app.applicant_email = email
    app.job_offer = offer
    app.description_postulant = description_postulant
    app.date = date
    app
  end

  def self.find_by_job_offer(job_offer)
    JobApplication.where(job_offer: job_offer)
  end

  def process
    JobVacancy::App.deliver(:notification, :contact_info_email, self)
    JobVacancy::App.deliver(:notification, :description_info_email, self)
  end
end
