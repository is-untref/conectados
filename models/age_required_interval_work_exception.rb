class AgeRequiredIntervalWorkException < StandardError
  def initialize
    raise 'The required age is outside the working age range. It must be up to sixty-five years..'
  end
end
