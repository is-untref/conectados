require_relative '../models/date_birth_interval_work_exception'

class AnalyzerOfDateBirth
  def date_validate(date)
    f_date = Date.strptime(date, '%Y-%m-%d')
    DateBirthIntervalWorkException.new if f_date.year > (Date.today.year - 18)
    DateBirthIntervalWorkException.new if f_date.year < (Date.today.year - 65)
  end
end
