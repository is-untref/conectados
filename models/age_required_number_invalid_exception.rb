class AgeRequiredNumberInvalidException < StandardError
  def initialize
    raise 'The required age must be greater than or equal to eighteen.'
  end
end
