class RemunerationNegativeException < StandardError
  def initialize
    raise 'The remuneration can not be negative.'
  end
end
