require_relative '../models/age_required_number_invalid_exception'
require_relative '../models/age_required_interval_work_exception'

class AnalyzerOfAgeRequired
  def analyze_age_required(age)
    AgeRequiredNumberInvalidException.new if age.to_i < 18 && age.to_i != 0
    AgeRequiredIntervalWorkException.new if age.to_i > 65 && age.to_i != 0
  end
end
