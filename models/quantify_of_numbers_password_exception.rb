class QuantifyOfNumbersPasswordException < StandardError
  def initialize
    raise 'Password invalid. It should have at least 4 numbers.'
  end
end
