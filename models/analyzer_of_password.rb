require_relative '../models/size_of_password_exception'
require_relative '../models/quantify_of_numbers_password_exception'

class AnalyzerOfPassword
  def analyze_size(password)
    SizeOfPasswordException.new if password.length < 10
  end

  def analyze_numbers(password)
    QuantifyOfNumbersPasswordException.new if password.count('0-9') < 4
  end

  def compare_passwords(password, password_confirmation)
    raise 'Passwords do not match' if password != password_confirmation
  end
end
