class JobOffer < Sequel::Model
  many_to_one :user
  one_to_many :job_applications

  def validate
    super
    validates_presence :title
  end

  def owner
    user
  end

  def owner=(a_user)
    self.user = a_user
  end

  def self.all_active
    JobOffer.where(is_active: true)
  end

  def self.find_by_owner(user)
    JobOffer.where(user: user)
  end

  def self.deactivate_old_offers
    JobOffer.all_active.each do |offer|
      if (Date.today - offer.updated_on) >= 30
        offer.deactivate
        offer.save
      end
    end
  end

  def activate
    self.is_active = true
  end

  def deactivate
    self.is_active = false
  end

  def add_postulant(id)
    quantity_of_postulations = JobOffer.with_pk(id).quantity_of_postulations

    if quantity_of_postulations.nil?
      quantity_of_postulations = 1
    else
      quantity_of_postulations += 1
    end

    JobOffer.with_pk(id).update(quantity_of_postulations: quantity_of_postulations)
  end
end
