class SizeOfPasswordException < StandardError
  def initialize
    raise 'Password invalid. It should have a size longer than 10.'
  end
end
