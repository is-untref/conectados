class RemunerationNumberException < StandardError
  def initialize
    raise 'The remuneration has to be a number.'
  end
end
