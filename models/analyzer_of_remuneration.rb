require_relative '../models/remuneration_negative_exception'
require_relative '../models/remuneration_number_exception'

class AnalyzerOfRemuneration
  def analyze_remuneration(remuneration)
    RemunerationNegativeException.new if remuneration.to_i.negative?
    RemunerationNumberException.new if remuneration =~ /[a-z]/ || remuneration =~ /[A-Z]/
  end
end
